/*
Problem 1: Print the chess board pattern
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
*/
function printPattern(){
    let arr = [' ', '#']; 
    let flag = true;

    for(let i=0; i < 8; i++){
    let output = '';
    for(let j=0; j<8; j++){
        let rem = j % 2;
        if(flag){
        output += arr[rem];
        } else {
        output += arr[1-rem];
        }
    }
    console.log(output)
    flag = !flag;
    }
}


/*
Problem 2: How to get the target number by using 'multiply by 3' or 'add 5' 
//eg: 13 -> 1 * 3 + 5 + 5  
2 options, add 5 or multiply by 3

1 * 3 > target, exit
1 + 5

*/
function findSolution(target){

  function find(input, text){
    if(target === input){
      return text;
    } else if(input > target){
      return null;
    } else {
      return find(input * 3, "( " + text + " * 3 )") || find(input + 5, text + " + 5");
    }
    
  }
  return find(1, "1");
}