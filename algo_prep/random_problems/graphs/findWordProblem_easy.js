/*
A precedence rule is given as "P>E", which means that letter "P" is followed directly by the letter "E".
Write a function, given an array of precedence rules, that finds the word represented by the given rules.

Note: Each represented word contains a set of unique characters, i.e. the word does not contain duplicate letters.

Examples:

findWord(["P>E","E>R","R>U"]) // PERU

findWord(["I>N","A>I","P>A","S>P"]) // SPAIN

Test cases:

findWord(["U>N", "G>A", "R>Y", "H>U", "N>G", "A>R"]) // HUNGARY

findWord(["I>F", "W>I", "S>W", "F>T"]) // SWIFT

findWord(["R>T", "A>L", "P>O", "O>R", "G>A", "T>U", "U>G"]) // PORTUGAL

findWord(["W>I", "R>L", "T>Z", "Z>E", "S>W", "E>R", "L>A", "A>N", "N>D", "I>T"]) // SWITZERLAND  

*/


function findWord(arr){
  let graph1 = {}; let graph2 = {};
  for(let i=0; i<arr.length;i++){
    let output = arr[i].split(">");
    graph1[output[0]]= output[1];
    graph2[output[1]]= output[0];
  }
  let start = arr[0].split(">")[1]; //E
  while(graph2[start]){
    start = graph2[start];
  }
  let output = ""+start;
  while(graph1[start]){
    output += graph1[start];
    start = graph1[start];
  }
  return output;
}
