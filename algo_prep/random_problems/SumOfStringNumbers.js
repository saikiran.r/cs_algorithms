// input '456', '123' -> output '579'

function sumOfStringNumbers1(n1, n2) {
  var i = n1.length-1;
  var j= n2.length-1; //2,2
  var result="";
  var carry = false;
  while(i>=0 || j>=0){
    let subNum1 = i >= 0 ? (n1[i] - '0') : 0;//4
    let subNum2 = j >= 0 ? (n2[j] - '0') : 0;//4
    total = (carry?1:0) + subNum1 + subNum2;//11
    carry = total >= 10;//true
    result = total % 10 + result;//12
    i--;
    j--;
  }
  if(carry){
    result = 1 + result;
  }
  return result;
}

function sumOfStringNumbers2(a, b){
  let i = a.length -1; 
  j= b.length -1;//2,2
  result='';
  let carry = 0
  while(i >=0 || j>=0){
    let num1 = (a[i] - '0') ? (a[i] - '0') : 0; //6
    let num2 = (b[j] - '0') ? (b[j] - '0') : 0; //3
    add = carry + num1+num2;
    carry = (add >=10) ? 1 : 0;
    result = add % 10 + result;
    i--; 
    j--;
  }
  return carry ? carry + result : result;
}

function sum(a, b) {
  let result = '';
  let carrying = false;
  while (a.length || b.length) {
    let num =
      parseInt(a.substring(a.length - 1) || 0) +
      parseInt(b.substring(b.length - 1) || 0) +
      (+carrying);
    carrying = (num >= 10);
    result = `${num.toString().split('')[1] || num}${result}`;
    a = a.slice(0, -1);
    b = b.slice(0, -1);
  }
  return carrying ? `1${result}` : result;
}
