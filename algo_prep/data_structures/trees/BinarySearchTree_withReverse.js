/*
  10
9    11
ref: https://repl.it/@aneagoie/Data-Structures-Trees#main.js
viso: https://visualgo.net/en/bst
*/

class Node{
  constructor(value){
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor(){
    this.root = null;
  }

  insert(value){
    let newNode = new Node(value);
    if(!this.root){
      this.root = newNode;
      return this;
    }
    let node = this.root;
    while(true){
      if(value < node.value) {
        if(!node.left) {
          node.left = newNode;
          return this;
        }
        node = node.left;
      } else {
        if(!node.right) {
          node.right = newNode;
          return this;
        }
        node = node.right;
      }
    }
  }

  lookup(value){
    let currentNode = this.root;
    while(currentNode){
      if(value === currentNode.value){
        return currentNode;
      }
      if(value < currentNode.value) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
    }
    return null;
  }

  traverse(node){
    const tree = {value: node.value};
    tree.left = node.left === null ? null : traverse(node.left);
    tree.right = node.right === null ? null : traverse(node.right);
    return tree;
  }

  remove(value){

    if(!this.root){
      return false;
    }
    let currentNode = this.root;
    let parentNode = null;
    childDirection = null;
    while(currentNode) {
      if (currentNode.value == value) {
        //Do the task. remove node as there is a match

        

        // match node has no children
        

        // match node has no right children


        //match node has no left children



        //match node has both children
        

        
        let newNodeInPlace = getNodeToAttach(currentNode)
        parentNode[childDirection] = appendRemovedChildren(newNodeInPlace, currentNode);
        removeNode(parentNode, currentNode, childDirection);
      }
      parentNode = currentNode;
      if(value < currentNode.value){
        currentNode = currentNode.left;
        childDirection = "left"
      } else{
        currentNode = currentNode.right;
        childDirection = "right"
      }
    }
    return null;
  }

  removeNode(parentNode, node, childDirection){
    
    if(!node.left && !node.right) {
      parentNode[childDirection] = null;
    } else if(!node.left && node.right) {
      parentNode[childDirection] = node.right;
    } else if(node.left && !node.right) {
      parentNode[childDirection] = node.left;
    } else {
      node = node.right;
    }
  } 

  appendRemovedChildren(newNode, removableNode){
    if(!node){
      return node;
    } else if(node.left){
      node.right = removableNode.right;
    } else {
      node.left = removableNode.left;
    }
  }

  getNodeToAttach(node){
    let childNodes = getChildNodes(node);
    if(childNodes.length == 0){
      return null;
    } else if(childNodes.length == 1){
      return node[childNodes[0]];
    } else {
      node = node.right;
      while(node) {
        childNodes = getChildNodes(node);
        if(childNodes.length == 0){
          return node;
        } else if(childNodes.length == 1){
          return node[childNodes[0]];
        } else {
          node = node.left;
        }
      }
    }
    
  }

  getChildNodes(node) {
    if(!node.left && !node.right) {
      return [];
    } else if(!node.left && node.right) {
      return ["right"];
    } else if(node.left && !node.right) {
      return ["left"];
    } else {
      return ["left","right"];
    }
  }

  
}



const tree = new BinarySearchTree();
tree.insert(9)
tree.insert(4)
tree.insert(6)
tree.insert(20)
tree.insert(170)
tree.insert(15)
tree.insert(1)

