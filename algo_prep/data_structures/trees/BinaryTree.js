/*

      1
   2    3 
 4  5  6  7
8 9

to 

    1
  3    2 
7  6  5  4
        8 9


*/


// ref: https://repl.it/@aneagoie/Data-Structures-Trees-exercise-2#main.js

class Node{
  constructor(value){
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor(){
    this.root = null;
  }

  insert(value){
    let newNode = new Node(value);
    if(!this.root){
      this.root = newNode;
      return this;
    }
    let node = this.root;
    while(true){
      if(value < node.value) {
        if(!node.left) {
          node.left = newNode;
          return this;
        }
        node = node.left;
      } else {
        if(!node.right) {
          node.right = newNode;
          return this;
        }
        node = node.right;
      }
    }
  }

  lookup(value){
    let currentNode = this.root;
    while(currentNode){
      if(value === currentNode.value){
        return currentNode;
      }
      if(value < currentNode.value) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
    }
    return null;
  }

  traverse(node){
    const tree = {value: node.value};
    tree.left = node.left === null ? null : traverse(node.left);
    tree.right = node.right === null ? null : traverse(node.right);
    return tree;
  }

  remove(value){
    // it is complex
  }
}

const tree = new BinarySearchTree();
tree.insert(9)
tree.insert(4)
tree.insert(6)
tree.insert(20)
tree.insert(170)
tree.insert(15)
tree.insert(1)



function reverseTree(node){
  current = node;
  if (current) {
    let temp = current.left;
    current.left = current.right
    current.right = temp;
    reverseTree(current.left);
    reverseTree(current.right);
  }
  return node;
}