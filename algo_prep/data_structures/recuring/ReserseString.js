//https://stackoverflow.com/questions/105838/real-world-examples-of-recursion

function reverseStringItr(str) {
  let result = "";
  for(let i= str.length-1; i >= 0 ; i--){
    result += str[i];
  }
  return result;
}
reverseStringItr('yoyo mastery')

function reverseString(str){
  let length = str.length;
  if(length == 1) {
    return str;
  }
  return str[length -1] + reverseString(str.substr(0, length-1));
}
reverseString('yoyo mastery')
