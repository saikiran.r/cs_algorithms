function findFactorialRecursion(n){
  if(n <= 1){
    return 1;
  }
  return n * findFactorialRecursion(n-1);
}
  
function findFactorialIterative(n){
  let fact = 1;
  while(n >1){
    fact *= n;
    n--;
  }
  return fact;
}
