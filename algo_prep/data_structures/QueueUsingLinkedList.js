class Node = {
  constructor(value){
    this.value = value;
    this.next  = null;
  }
}

class Queue{
  constructor(){
    this.last = null;
    this.first = null;
    this.length = 0;
  }
  
  peek(){
    return this.first;
  }
  
  enQueue(value){
    let newNode = new Node(value);
    if(!this.first){
      this.first = newNode;
    } else {
      this.last.next  = newNode;
    }
    this.last = newNode;
    this.length++
    return this;
  };
  
  deQueue(){
    if(!this.first){
      return null;
    }
    let node = this.first;
    if(this.first == this.last){
      this.last = null;
    }
    this.first =  this.first.next;
    this.length--;
    return node;
  };
}

const myQueue = new Queue();
myQueue.peek();
myQueue.enqueue('Joy');
myQueue.enqueue('Matt');
myQueue.enqueue('Pavel');
myQueue.peek();
myQueue.dequeue();
myQueue.dequeue();
myQueue.dequeue();
myQueue.peek();