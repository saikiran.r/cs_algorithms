/*

1,2,3,4,5,

5,4,3,2,1

*/

class Stack(
  constructor(){
    this.data = [];
  }
  peek(){
    return this.data[this.data.length-1];
  }
  pop(){
    this.data.pop();
    return this;
  }
  push(value){
    this.data.push(value);
    return this;
  }
  isEmpty(){
    return this.data.length ? 1: 0; 
  }
)

class Queue{
  constructor(){
    this.first = null;
    this.last = null;
    this.length = 0;
    this.data = new Stack();
  }
  
  peek(){
    return this.data.bottom;
  }
  
  push(value){
    this.data.push(value);
    return this;
  }
  
  pop(){
    if(this.data.length === 0){
      return null
    }
    if(this.data.length == 1){
      this.data.pop();
      return this;
    }
    let temp = new Stack();
    while(this.data.peek()){
      temp.push(this.data.pop());
    }
    removedNode = temp.pop();
    while(temp.peek()){
      this.data.push(temp.pop());
    }
    return removedNode; 
  }
  
  isEmpty(){
    this.data.isEmpty();
  }
}