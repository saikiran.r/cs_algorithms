class Node{
  constructor(value){
    this.value = value;
    this.next = null;
    this.prev = null; //DLL
  }
}

class LinkedList{
  constructor(value){
    this.head = new Node(value);
    this.tail = this.head;
    this.length = 1;
  }

  prepend(value){
    var newNode = new Node(value);
    newNode.next = this.head;
    this.head.prev = newNode; //DLL
    this.head = newNode;
    this.length++;
    return this;
  }

  append(value){
    var newNode = new Node(value);
    this.tail.next = newNode;
    newNode.prev = this.tail; //DLL
    this.tail = newNode;
    this.length++;
    return this;
  }

  printList(){
  let array = [];
    currentNode = this.head;
    while(currentNode){
      array.push(currentNode.value);
      currentNode = currentNode.next;
    }
    return array;
  }

  insert(index, value){
    if(index == 0){
      return this.prepend(value);
    }
    if(index >= this.length){
      return this.append(value);
    }
    var newNode = new Node(value);
    let leader = this._getNodeWithIndex(index - 1);
    let successor = leader.next;
    newNode.next = leader.next;
    newNode.prev = leader;
    successor.prev = newNode;
    leader.next = newNode;
    this.length++;
    return newNode;
  }

  remove(index){
    if(index == 0){
      this.head = this.head.next;
      this.head.prev = null;
      return this.printList();
    }
    if(index > this.length){
      return undefined;
    }
    let leader = this._getNodeWithIndex(index - 1);
    let currentNode = leader.next;
    let successor = currentNode.next;
    successor.prev = leader; // DLL
    leader.next = currentNode.next;
    this.length--;
    return this.printList;
  }

  indexOf(value){
    var currentNode = this.head;
    let index = 0;
    while(currentNode) {
      if(currentNode.value === value){
        return index;
      }
      index++;
      currentNode = currentNode.next;
    } 
    return undefined;
  }

  get(index){
    let node = this._getNodeWithIndex(index);
    return node.value;
  }

  set(index, value){
    let node = this._getNodeWithIndex(index);
    node.value = value;
    return node;
  }

  _getNodeWithIndex(index){
    var currentNode = this.head;
    let i = 0;
    if(index < 0 || index >= this.length){
      return undefined;
    }
    while(currentNode) {
      if(i === index){
        return currentNode;
      }
      i++;
      currentNode = currentNode.next;
    } 
    return undefined;
  }

  
}