/*

Note: this solution is making push o(n) but others are faster.
The previous one is making push O(1) but other functions are o(n)

1,2,3,4,5,

5,4,3,2,1

*/

class Stack(
  constructor(){
    this.data = [];
  }
  peek(){
    return this.data[this.data.length-1];
  }
  pop(){
    this.data.pop();
    return this;
  }
  push(value){
    this.data.push(value);
    return this;
  }
  isEmpty(){
    return this.data.length ? 1: 0; 
  }
)

class Queue{
  constructor(){
    this.stack1 = new Stack();
    this.stack2 = new Stack();
  }
  
  peek(){
    return this.stack2.peep();
  }
  
  push(value){
    while(this.stack2.peep()){
      this.stack1.push(this.stack2.pop());
    }
    this.stack1.push(value);
    while(this.stack1.peep()){
      this.stack2.push(this.stack1.pop());
    }
    return value;
  }
  
  pop(){
    return this.stack2.pop();
  }
  
  isEmpty(){
    return this.stack2.isEmpty() && this.stack1.isEmpty();
  }
}