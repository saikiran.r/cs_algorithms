Resume cheet Sheet - https://github.com/aneagoie/resume-checklist

Big O cheat sheet - https://www.bigocheatsheet.com/
Master the interview - https://coggle.it/diagram/W5E5tqYlrXvFJPsq/t/master-the-interview-click-here-for-course-link
https://coggle.it/diagram/W5u8QkZs6r4sZM3J/t/master-the-interview

List fo Data Structures - https://en.wikipedia.org/wiki/List_of_data_structures

viso for Linked list - https://visualgo.net/en/list?slide=1



// Trees: 
1. Binary Tree - tree having not more than 2 children O(log n)
    Perfect Binary tree and full Binary tree
    - Binary Search Tree = Binary tree with a condition i.e. left  nodes less than node value, and right nodes greater than the node value.
      Balanced Binary search tree and Unbalanced search tree
      balanced Binary tree by using AVL trees or red black trees




Resources: AVL Trees + Red Black Trees
If you would like to learn more about these types of trees where the complexity comes from the auto-balancing of trees upon insertion, these are the resources that I recommend but keep in mind that they are not SUPER important for an interview. Just to scratch your curiosity :)

AVL Trees:

Animation - https://www.cs.usfca.edu/~galles/visualization/AVLtree.html
How it Works - https://medium.com/basecs/the-little-avl-tree-that-could-86a3cae410c7

Red Black Trees:

Animation - https://www.cs.usfca.edu/~galles/visualization/RedBlack.html
How it Works - https://medium.com/basecs/painting-nodes-black-with-red-black-trees-60eacb2be9a5

You can compare the technical details between the two here - https://stackoverflow.com/questions/13852870/red-black-tree-over-avl-tree


Binary Heap Tree - https://visualgo.net/en/heap?slide=1


--------------------------
Algo Graphs - https://visualgo.net/en/graphds?slide=1

Types of graphs
1. Undirectional and Directional graphs
2. Unweighted and Weighted graphs
3. Acyclic and Cyclic graphs

graph data can be represented /built by using other data structures 
1. Edge List (Using arrays) - [[0, 2], [2, 3], [2, 1], [1, 3]]
2. Adjacent List (Using objects or arrays) 
    - [[2], [2, 3], [0, 1, 3], [1, 3]]
    - {
        0: [2], 
        1: [2, 3], 
        2: [0, 1, 3], 
        3: [1, 3]
      }

3.  Adjacent Matrix (Using objects or arrays) 
    - [
        [0, 0, 1, 0],
        [0, 0, 1, 1],
        [1, 1, 0, 1],
        [0, 1, 1, 0]
      ]
 