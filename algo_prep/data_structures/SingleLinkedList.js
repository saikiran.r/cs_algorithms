class Node{
  constructor(value){
    this.value = value;
    this.next = null;
  }
}

class LinkedList{
  constructor(value){
    this.head = new Node(value);
    this.tail = this.head;
    this.length = 1;
  }

  prepend(value){
    var newNode = new Node(value);
    newNode.next = this.head;
    this.head = newNode;
    this.length++;
    return this;
  }

  append(value){
    var newNode = new Node(value);
    this.tail.next = newNode;
    this.tail = newNode;
    this.length++;
    return this;
  }

  printList(){
  let array = [];
    currentNode = this.head;
    while(currentNode){
      array.push(currentNode.value);
      currentNode = currentNode.next;
    }
    return array;
  }

  insert(index, value){
    if(index == 0){
      return this.prepend(value);
    }
    if(index >= this.length){
      return this.append(value);
    }
    var newNode = new Node(value);
    let leader = this._getNodeWithIndex(index - 1);
    newNode.next = leader.next;
    leader.next = newNode;
    this.length++;
    return newNode;
  }

  remove(index){
    if(index == 0){
      this.head = this.head.next;
    }
    if(index > this.length){
      return undefined;
    }
    let leader = this._getNodeWithIndex(index - 1);
    let currentNode = leader.next;
    leader.next = currentNode.next;
    this.length--;
    return currentNode;
  }

  indexOf(value){
    var currentNode = this.head;
    let index = 0;
    while(currentNode) {
      if(currentNode.value === value){
        return index;
      }
      index++;
      currentNode = currentNode.next;
    } 
    return undefined;
  }

  get(index){
    let node = this._getNodeWithIndex(index);
    return node.value;
  }

  set(index, value){
    let node = this._getNodeWithIndex(index);
    node.value = value;
    return node;
  }

  _getNodeWithIndex(index){
    var currentNode = this.head;
    let i = 0;
    if(index < 0 || index >= this.length){
      return undefined;
    }
    while(currentNode) {
      if(i === index){
        return currentNode;
      }
      i++;
      currentNode = currentNode.next;
    } 
    return undefined;
  }

  reverse() {
      if (!this.head.next) {
        return this.head;
      }
      let first = this.head;
      this.tail = this.head;
      let second = first.next;
  
      while(second) {
        const temp = second.next;
        second.next = first;
        first = second;
        second = temp;
      }
  
      this.head.next = null;
      this.head = first;
      return this.printList();
    }
  
}

let myLinkedList = new LinkedList(10);
myLinkedList.append(5);
myLinkedList.append(16);myLinkedList.prepend(1);
myLinkedList.insert(2, 99);
myLinkedList.insert(20, 88);
myLinkedList.remove(2);
myLinkedList.reverse();

