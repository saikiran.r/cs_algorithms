// implement an array in JS

class MyArray{
  constructor(){
    this.data = {};
    this.length = 0;
  }

  get(index){
    return this.data[index];
  }

  push(value) {
    this.data[this.length] = value;
    this.length++;
    return this.data;
  }

  pop(){
    let val = this.data[this.length-1];
    delete this.data[this.length-1];
    this.length--;
    return val;
  }

  indexOf(value) {
    for (key in this.data) {
      if(a[key] === value){
        return key;
      }
    }
    return undefined;
  }

  insert(index, value){
    for(let i= this.length; i>index; i--){
      this.data[i] = this.data[i-1];
    }
    this.data[index] = value;
    this.length++;
    return value;
  }

  delete(index){
    if(index >= this.length || index < 0){
      return undefined;
    }
    const value = this.data[index];
    for(let i=index; i<this.length-1; i++){
      this.data[i] = this.data[i+1];
    }
    delete this.data[this.length-1];
    this.length--;
    return value;
  }
}