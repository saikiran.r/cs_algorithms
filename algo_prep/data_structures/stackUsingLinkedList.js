class Node {
  constructor(value){
    this.value = value;
    this.next = null;
  }
}

class Stack {
  constructor(){
    this.top = null;
    this.bottom = null;
    this.length = 0;
  }
  peek() {
    return this.top.value;
  }
  push(value){
    let newNode = new Node(value);
    newNode.next = this.top;
    if(this.length == 0){
      this.bottom = newNode;
    }
    this.top = newNode;
    this.length++;
    return newNode;
  }
  pop(){
    if(this.length == 0){
      return undefined;
    }
    let node = this.top;
    this.top = node.next;
    this.length--;
    if(this.length == 0){
      this.bottom = this.top;
    }
    return node.value;
  }

  isEmpty(){
    return !this.length? true : false;
  }
  //isEmpty
}

const myStack = new Stack();




//Discord
//Udemy
//google
