https://stackoverflow.com/questions/1517793/what-is-stability-in-sorting-algorithms-and-why-is-it-important


List of sorting

insertionSort - This needs to be used when the array is very small and mostly sorted Data
bubbleSort - never use it
selectionSort - never use it
mergeSort - Best but space complexity is O(n)
quickSort - Best and also space complexity is o(log(n)) but worst case is o(n^2)
https://repl.it/@aneagoie/quickSort#main.js


heapsort - https://brilliant.org/wiki/heap-sort/
https://stackoverflow.com/questions/2467751/quicksort-vs-heapsort

Radix Sort: https://brilliant.org/wiki/radix-sort/

Radix Sort Animation: https://www.cs.usfca.edu/~galles/visualization/RadixSort.html



Counting Sort: https://brilliant.org/wiki/counting-sort/

Counting Sort Animation: https://www.cs.usfca.edu/~galles/visualization/CountingSort.html
