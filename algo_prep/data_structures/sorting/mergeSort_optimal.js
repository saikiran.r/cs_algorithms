function sort(array){
  if(!array?.length){
    return null;
  }
  if(array.length == 1){
    return array;
  }
  return mergeSort(array, 0, array.length -1);
}

function mergeSort(array, leftIndex, rightIndex){
  if(leftIndex < rightIndex){
    let middleIndex = Math.floor((leftIndex +  rightIndex) / 2);
    mergeSort(array, leftIndex, middleIndex);
    mergeSort(array, middleIndex+1, rightIndex);

    return merge(array, leftIndex, middleIndex, rightIndex);
  }
  return null;
}

// Merges two subarrays of arr[].
// First subarray is arr[l..m]
// Second subarray is arr[m+1..r]
function merge(array, l, m, r){

  // Find sizes of two subarrays to be merged
  var leftArrayLength = m - l + 1;
  var rightArrayLength = r - m;

  /* Create temp arrays */
  var leftArray = new Array(leftArrayLength);
  var rightArray = new Array(rightArrayLength);

  /*Copy data to temp arrays*/
  for(let i=0 ; i< leftArrayLength; i++){
    leftArray[i] = array[l + i];
  }

  for(let j=0 ; j< rightArrayLength; j++){
    rightArray[j] = array[m + 1 + j];
  }

  /* Merge the temp arrays */
  let i=0, j = 0, k = l;
  while(i < leftArrayLength && j < rightArrayLength) {
    if(leftArray[i] <= rightArray[j]){
      array[k] =  leftArray[i];
      i++;
    } else {
      array[k] = rightArray[j];
      j++;
    }
    k++;
  }

  while(i < leftArrayLength){
    array[k] = leftArray[i];
    k++;
    i++;
  }

  while(j < rightArrayLength){
    array[k] = rightArray[j];
    k++;
    j++;
  }  
  return array;
}

let numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];
sorrt(numbers);