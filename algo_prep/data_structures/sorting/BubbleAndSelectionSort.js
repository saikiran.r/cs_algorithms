let numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

/* 
bubbling the largest numbers to the right by comparing adjacent values of array:
*/
function bubbleSort(array){
  const length = array.length;
  for(let i=0; i< length; i++) {
    let swapped = false;
    for(let j=0; j < length - i; j++) {
      if(array[j] > array[j+1]){
        let temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
        swapped = true;
      }
    }
    if (!swapped){
      break;
    }
  }
  return array;
}
bubbleSort(numbers);

/* pushing the min numbers to the left:
Find the min value out of all arrays and replace swap 1st element with the min index position,
Find the next min value again and swap 
 */
function selectionSort(array){
  const length = array.length;
  for(i = 0; i< length; i++){
    let minIndex = i; 
    for(j = i+1; j < length; j++){
      if(array[j] < array[minIndex]) {
        minIndex = j;
      } 
    }
    if(i != minIndex){
      temp = array[i];
      array[i] = array[minIndex];
      array[minIndex] = temp;
    }
  }
  return array;
}