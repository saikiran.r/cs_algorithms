// Used slice here to split arrays 
const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

function mergeSort(array){
  const length = array.length;
  if(length == 1){
    return array;
  }

  let half = Math.floor(length / 2);
  let left = array.slice(0, half);
  let right = array.slice(half)

  return merge(
    mergeSort(left),
    mergeSort(right)
  )
  
}

function merge(array1, array2){
  const result = [];
  let i=0, j=0;
  while(i < array1.length && j < array2.length){
    if(array1[i] < array2[j]){
      result.push(array1[i]);
      i++;
    } else {
      result.push(array2[j]);
      j++;
    }
  }

  while( i < array1.length) {
    result.push(array1[i]);
    i++;
  }

  while( j < array2.length) {
    result.push(array2[j]);
    j++;
  }
  
  return result;
}


mergeSort(numbers);