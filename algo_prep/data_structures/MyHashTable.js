
/*
[
  [["a", "valueOfA"]],
  [["b", "valueOfB"]],
]
*/

class HashTable {
  constructor(size){
    this.data = new Array(size);
  }

  _hash(key){
    let hash = 0;
    // adding all the ASCII values of key and divide by length to make it unique
    for(i=0; i<key.length; i++) {
      hash = (hash + key.charCodeAt(key[i]) * (i+1)) % this.data.length
    }
    return hash;
  }

  get(key) {
    let hash = _hash(key);
    for(let i = 0; i < this.data[hash].length; i++){
      if(this.data[hash][i][0] === key) {
        return this.data[hash][i][1];
      }
    }
    return undefined;
  }

  set(key, value){
    let hash = _hash(key);
    if(!this.data[hash]){
      this.data[hash] = [];
    } 
    this.data[hash].push([key, value]);
    return this.data;
  }

  delete(key) {
    let hash = _hash(key);
    if(!this.data[hash].length){
      return null;
    } else if(this.data[hash].length == 1){
      this.data[hash] = [];
      return key;
    } else {
      for(let i = 0; i < this.data[hash].length; i++){
        if(this.data[hash][i][0] === key) {
          this.data[hash].delete(i);
          return key;
        }
      }
    }
    return undefined;
  }

  keys(){
    let result = [];
    for(let i=0; i<this.data; i++) {
      for(let j=0; j<this.data[i]; i++) {
        result.push(this.data[i][j][0]);
      }
    }
    return result;
  }
}