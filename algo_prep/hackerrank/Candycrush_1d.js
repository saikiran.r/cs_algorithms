/*

https://leetcode.com/discuss/interview-question/380650/Bloomberg-or-Phone-Screen-or-Candy-Crush-1D

*/

//GreedyMethod
function removeString(str){
  debugger;
  if(str.length <= 2){
    return str;
  }
  let char = str[0]; //b
  let count = 1;
  let i= 1;
  let startIndex = 0;
  while(i < str.length){ //7
    if(str[i] == char){
      count++; //3
      i++; //5
      if(i != str.length){
        continue;
      }
    }
    
    if(count >= 3){
      return removeString(str.substring(0, startIndex) + str.substring(i)); 
    } else {
      startIndex = i; //2
      count = 1; //1
      char = str[i];
      i++; //3
    }
    
  }
  return str;
}


//FollowUp shortest string method

function removeString(str, startInt = 0){
  debugger;
  if(str.length <= 2){
    return str;
  }
  let char = str[startInt]; //b
  let count = 1;
  let i= startInt + 1;
  let startIndex = startInt;
  while(i < str.length){ //7
    if(str[i] == char){
      count++; //3
      i++; //5
      if(i != str.length){
        continue;
      }
    }
    
    if(count >= 3){
      //2 options, remove them or ignore them and continue
      let newStr = removeString(str.substring(0, startIndex) + str.substring(i));
      let newStr2 = removeString(str, i);
      if(newStr.length < newStr2.length ){
        return newStr
      } else {
        return newStr2;
      }
      //return removeString(str.substring(0, startIndex) + str.substring(i)); 
    } else {
      startIndex = i; //2
      count = 1; //1
      char = str[i];
      i++; //3
    }
    
  }
  return str;
}