/*
476. Number Complement
https://leetcode.com/problems/number-complement/
*/


// solution 3: Using masking and left shift.

/**
 * @param {number} num
 * @return {number}
 */
var findComplement = function(num) { 
    // eg: num 5
    if(num == 0) return 1;
    const noOfBinaryDigits = parseInt(Math.log2(num)) + 1; //eg: log10(1000) = 3+1 = 4 digits in 1000 number
    const mask = (1 << noOfBinaryDigits) - 1; // left shift noOfBinaryDigits times. eg: 10 << 3 is 10000
    return mask ^ num;
    
};





//Solution 2 using bitwise operators and shifts, space o(1), time o(log2N)

/**
 * @param {number} num
 * @return {number}
 */
var findComplement = function(num) {
    // eg: num 5
    if(num == 0){
        return 1;
    }
    let newNum = 0;
    let multiplier = 1;
    while(num){
        let bit = num & 1; // this returns the last binary digit , Ans: 1, 0, 1
        num = num >> 1; // right shift. eg: makes 111 to 11 to 1 to 0
        
        newNum += (1-bit) * multiplier; // complement and multiple with 2 pow n
        multiplier = multiplier << 1; // left shift. eg: 10 to 100 to 1000
    }
    return newNum;
};






// Basic Solution 1
/**
 * @param {number} num
 * @return {number}
 */
var findComplement = function(num) {
    const binary = convertBase10IntoBinary(num);
    const comp = complementBinary(binary);
    return convertBinaryIntoBase10(comp);
};

function convertBase10IntoBinary(num){
    let str = "";
    while(num != 0){
        str = num % 2 + str;
        num = parseInt(num / 2);
    }
    return str;
}

function complementBinary(binaryNum){
    let newStr = "";
    for(let i=0; i< binaryNum.length; i++){
        newStr +=  (binaryNum[i] == 1) ? "0" : "1";
    }
    return newStr;
}

function convertBinaryIntoBase10(binaryNum){
    let n = binaryNum.length;
    let num = 0;
    for(let i=0; i< binaryNum.length; i++){
        num +=  binaryNum[i] * Math.pow(2, n - 1 - i);
    }
    return num;
}