/*
3. Longest Substring Without Repeating Characters
https://leetcode.com/problems/longest-substring-without-repeating-characters/
*/
/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
    let res = 0;
    let map={};
    let start = 0;
    let i= 0;
    while(start >= 0 && i < s.length && start < s.length){
        let tempRes = (i - start) + 1; //1
        let char = s[i]; //b
        if(map[char]){
            
            while(start < i){
                let startChar = s[start]; 
                start++;//1
                if(startChar == char){
                    i++;
                    break;
                } else {
                    delete map[startChar];
                }
                
            }
        } else {
            map[char] = 1;
            i++; //3
            res = Math.max(res, tempRes); //3
        }
    }
    return res;
};