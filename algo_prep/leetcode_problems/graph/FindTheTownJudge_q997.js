/*
Q: 997. Find the Town Judge
https://leetcode.com/problems/find-the-town-judge
*/
// solution 1

/**
 * @param {number} N
 * @param {number[][]} trust
 * @return {number}
 */
var findJudge = function(N, trust) {
    if(N == 0 || !trust){
        return -1;
    }
    if(trust.length == 0 && N==1){
        return 1;
    }
    if(trust.length == 1){
        return trust[0][1];
    }
    let map = {};
    for(let i=0;i< trust.length; i++){
        if(!map[trust[i][1]]) {
            map[trust[i][1]] = [];
        }
        map[trust[i][1]].push(trust[i][0]); 
    }
    let judge = -1;
    for(let k in map) {
        if(map[k].length === N-1){
            judge = parseInt(k);
        }
    }
    for(let k in map) {
        if(k === judge){
            continue;
        }
        if(map[k].indexOf(judge) > -1){
            return -1;
        }
    }
    return judge;
};


//solution 2 - use two maps to avoid  indexOf
var findJudge = function(N, trust) {
    if(N == 0 || !trust){
        return -1;
    }
    if(trust.length == 0 && N==1){
        return 1;
    }
    if(trust.length == 1){
        return trust[0][1];
    }
    let map = {};
    let map2 = {};
    for(let i=0;i< trust.length; i++){
        if(!map[trust[i][1]]) {
            map[trust[i][1]] = [];
        }
        map[trust[i][1]].push(trust[i][0]); 
        map2[trust[i][0]] = true;
    }
    let judge = -1;
    for(let k in map) {
        if(map[k].length === N-1 && map2[k] !== true){
            judge = parseInt(k);
        }
    }
    return judge;
};
