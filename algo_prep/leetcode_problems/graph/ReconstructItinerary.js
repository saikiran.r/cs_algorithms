/*
332. Reconstruct Itinerary
https://leetcode.com/problems/reconstruct-itinerary/
*/

/**
 * @param {string[][]} tickets
 * @return {string[]}
 */
var findItinerary = function(tickets, start = "JFK") {
    let graphMap = convertIntoMap(tickets);
    let resultMap = new Array(tickets.length + 1); //6
    resultMap[0] = [start];
    return go(start, 1);
    
    
    function go(source, level){
        let dest = graphMap[source]; //["ATL", "SFO"], ["JFK", "SFO"]
        if(!dest){
            return null;
        }
        if(level > tickets.length + 1){ //1, 2 > 6
            return null;
        }
        for(let i=0; i<dest.length; i++){
        // while(dest && dest.length > 0) {
            let destLoc = dest.shift();
            resultMap[level] = [...resultMap[level-1], destLoc]; //1 [jfk, atl]
            if(resultMap[level].length === tickets.length + 1){
               return resultMap[level];
            }
            let ans = go(destLoc, level+1);
            if(!ans){
                dest.push(destLoc);
                continue;
            }
            return ans;
        }
        return null;
    }

    
};

function convertIntoMap(tickets){
    let map = {};
    for(let i=0; i<tickets.length; i++){
        let pair = tickets[i];
        if(!map[pair[0]]){
            map[pair[0]] = [pair[1]];
        } else {
            let arr = map[pair[0]];
            let j=0;
            for(; j<arr.length; j++){
                if(arr[j] < pair[1]){
                    continue;
                } else {
                    arr.splice(j, 0, pair[1]);
                    break;
                }
            }
            if(j == arr.length){
                arr.push(pair[1]);
            }
        }
    }
    return map;
}



/*

1. create a map from array. Insert values in acending order
    {   1: [2,3],
        2: [4],
        4: [3],
        3: [1]
    }
2. DFS
3. if count is input.length +1, then true. orelse null;


*/
