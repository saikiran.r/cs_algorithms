/*

https://leetcode.com/problems/house-robber/
198. House Robber

*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function(nums) {
    let pprev = 0, prev = 0, curr = 0;
    for(let i=0; i<nums.length; i++){
        curr = Math.max(prev, nums[i] + pprev);
        pprev = prev;
        prev = curr;
    }
    return curr;
};


/* 
Iterate through the array,
max of prev or pprev+currentValue -> this is the current max

*/