// q 41: Find the Missing Smallest Positive Number
https://leetcode.com/problems/first-missing-positive/
eg: input - [3, 4, 7, 1 ]   Ans - 2 (2 is missing number)


/* Solution Algorithm
Method 1: (Only if the inputs are +ve numbers, this won't work if the numbers are -ves / zeros)
O(N) time complexity  and O(1) space complexity
Step 1: Iterate through the array
Step 2: Get the element while iterating and make it's index value -ve. 
        Eg: 1st value is 3, so make 3rd element 7 * -1 = -7
            2nd value is 4,  so  make 4th element i *  -1 = -1, .....
Step 3: Iterate the array again after all modifications
Step 4: the first +ve value index is the Ans. In our example, the array would become [-3, 4, -7, -1]. As 2nd element (4) is +ve value, 2 is the Answer.


Method 2: This works always. (Note, we are starting index as 1  and not 0)
O(N) time complexity  and O(1) space complexity
Step 1: Iterate through the array
Step 2: swap the value to its index value. if the value is greater than N, then don't swap.
        Eg: 1st value is 3, so swap 3 to 3rd elements place [3, 4, 7, 1] becomes [7, 4, 3, 1]
Step 3: Again check if the value is less than N and swap if yes.
Step 4: Iterate the array again after all modifications
Step 5:  Now, the value and index value should be same. If not, that index is the answer.

*/

function findMSPN(array){
  if(array.length == 0){
      return 1;
  }
  for(let i=0; i< array.length; i++){
    let correctPos = array[i] - 1;
    while(array[i] <= array.length && array[i] !== array[correctPos]){ // true
      let temp = array[i];
      array[i] = array[temp - 1];  
      array[temp - 1] = temp; 
      correctPos = array[i] - 1;
    }
  }

  for(let i=0; i< array.length; i++){
    if(array[i] !== i+1 ){
      return i+1;
    }
  }
  return array.length+1;
}