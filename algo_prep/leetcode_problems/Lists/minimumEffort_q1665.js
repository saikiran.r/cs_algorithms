/*
1665. Minimum Initial Energy to Finish Tasks
https://leetcode.com/problems/minimum-initial-energy-to-finish-tasks/

*/

/**
 * @param {number[][]} tasks
 * @return {number}
 */
var minimumEffort = function(tasks) {
    let totalEnergy = 0;
    
    if(tasks.length == 1){
        return tasks[0][1];
    }
    //decending order based on diff btn initial energy and actual energy;
    tasks.sort((a,b) => (b[1] - b[0]) - (a[1] - a[0]))
    
    for(let i=0;i<tasks.length;i++){
        totalEnergy += tasks[i][0];
    }
    let result = totalEnergy;
    
    for(let i=0;i<tasks.length;i++){
        
        if(tasks[i][1] > totalEnergy){
            totalEnergy += tasks[i][1] - totalEnergy; //add diff in the tasks and totalenergy
        }
        totalEnergy -= tasks[i][0]; // remove actual energy to get the value that needs to be added to result
    }
    return result + totalEnergy; 
};