/*
238. Product of Array Except Self
https://leetcode.com/problems/product-of-array-except-self/
*/


// time complexity - o(n), space o(1)


/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function(nums) {
    let len = nums.length;
    
    let output = [1];
    for(let i=1; i< len; i++){
        output.push(output[i-1] * nums[i-1]);//1, 1, 2, 6
    }

    let prod = 1;
    for(let i=len-2; i>=0; i--){
        prod *= nums[i+1];
        output[i] *= prod;// 24, 12, 4, 1 // , , 8,6
    }
    return output;
};

// First solution: time complexity - o(n), space o(n)

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function(nums) {
    let len = nums.length;
    
    let prefixArray = [1];
    for(let i=1; i< len; i++){
        prefixArray.push(prefixArray[i-1] * nums[i-1]);//,,1, 1, 2, 6
    }
    
    let postfixArray = new Array(len);
    postfixArray[len-1] = 1;
    for(let i=len-2; i>=0; i--){
        postfixArray[i] = postfixArray[i+1] * nums[i+1];// 24, 12, 4, 1
    }
    
    let output = new Array(len);
    for(let i=0; i< len; i++){
        output[i] = prefixArray[i] * postfixArray[i];//
    }
    
    return output;
};
