/*
39. Combination Sum

https://leetcode.com/problems/combination-sum/
*/


/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
// without dp
var combinationSum = function(candidates, target) {
    let dp = [];
    let ans = go(candidates, 0, target, dp);
    return ans;
};

function go(candidates, currIndex, target, dp){
    debugger;
    if(currIndex >= candidates.length){
        return [];
    }
    if(target < 0){
        return [];
    }
    if(target == 0){
        return [[]];
    }
    
    let currentValue = candidates[currIndex]; //5
    // if(!dp[currIndex]){
    //     dp[currIndex] = [];
    // }
    // if(!dp[currIndex][target]){
        
        let ans =  go(candidates, currIndex + 1, target, dp) // skip adding current index

        let other = go(candidates, currIndex, target - currentValue, dp); // using the current index once
        
        // let result = [];
        
        for(let i=0; i< other.length; i++){
            other[i].push(currentValue);
        } 
        ans.push(...other);
        // result.push(...other);
        // result.push(...ans)
        
        // dp[currIndex][target] = ans;
        
    // }
    
    return ans;

}