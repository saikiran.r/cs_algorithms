/*
1. Two Sum
https://leetcode.com/problems/two-sum/

*/

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    debugger;
    if(nums.length <= 1){
        return [];
    }
    let map={}
    for(let i=0; i< nums.length; i++){//2
        let neededElem = target - nums[i]; //7
        if(!map[nums[i]]){
            map[neededElem]= i+1;
        } else {
            return [map[nums[i]]-1, i];
        }
    }
    return [];
    
};