/*
1288. Remove Covered Intervals
https://leetcode.com/problems/remove-covered-intervals/

*/
//solution 2: Optimal way

/**
 * @param {number[][]} intervals
 * @return {number}
 */
var removeCoveredIntervals = function(intervals) {
    
    intervals.sort(function(a,b) {
        if(a[0] === b[0]){
            return b[1] - a[1];
        }
        return a[0] - b[0];
    });
    let ans = intervals.length;
    let maxEnd = 0;
    for(let interval of intervals){
        let currEnd = interval[1];
        if(currEnd <= maxEnd){
            ans--;
        }
        maxEnd = Math.max(currEnd, maxEnd);
    }
    return ans;
};



    
    


//Brute force method o(n*2)

/**
 * @param {number[][]} intervals
 * @return {number}
 */
var removeCoveredIntervals = function(intervals) {
    let n = intervals.length;
    let result = [];
    for(let i = 0; i< n; i++){
        let overlap = false
        for(let j = 0; j< n; j++){
            if(i == j) {
                continue;
            }
            if(intervals[j][0] <= intervals[i][0] && intervals[i][1] <= intervals[j][1]){
                overlap = true; 
                break;
            }
        }
        if(!overlap){
            result.push(intervals[i]);
        }
    }
    return result.length;
};



    
    