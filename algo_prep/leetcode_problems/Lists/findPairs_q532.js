/*
532. K-diff Pairs in an Array
https://leetcode.com/problems/k-diff-pairs-in-an-array/
*/

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findPairs = function(nums, k) {
    if(nums.length == 1){
        return 0;
    }
    
    let map = {};
    for(let i=0; i< nums.length; i++){
        if(!map[nums[i]]){
            map[nums[i]] = 1;
        } else {
            map[nums[i]]++;
        }
    }
    
    let count = 0;
    for( let key in map){
        let value = parseInt(key);
       count += (k == 0) ? map[value] > 1 : map[value + k] >= 1;
    }
    
    return count;
    
};