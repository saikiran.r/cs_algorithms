//  121. Best Time to Buy and Sell Stock


/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    if(prices && prices.length <= 1){
       return 0;
    }
    let profit = 0;
    let min = prices[0];
    for(let i=1; i<prices.length; i++){
        if(prices[i] < min){
            min = prices[i];
        } else if(profit < (prices[i] - min)){
            profit = prices[i] - min;
        }
    }
    return profit;
};

maxProfit([7,1,5,3,6,4]); // ans: 5