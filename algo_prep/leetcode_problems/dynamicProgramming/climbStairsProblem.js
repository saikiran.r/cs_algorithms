//solution 1

var climbStairs = function(n) {
    
    let memo = new Array(n + 1);
    return climb_Stairs(0, n, memo);
  
};

function climb_Stairs(i, n, memo) {
    if (i > n) {
        return 0;
    }
    if (i == n) {
        return 1;
    }
    if (memo[i] > 0) {
        return memo[i];
    }
    memo[i] = climb_Stairs(i + 1, n, memo) + climb_Stairs(i + 2, n, memo);
    return memo[i];
}

//solution 2

var climbStairs = function(n) {
  let arr = [0, 1, 2];
  for(i=3; i<= n; i++){
    arr[i] = arr[i-1] + arr[i-2]; 
  }
  return arr[n];
};

//solution 3

function climbStairs(n){
  const arr = [0, 1, 2];  
  return climb(n, arr);
}

function climb(n, arr){
  if(arr[n]){
    return arr[n];
  }
  arr[n] = climb(n-1, arr) + climb(n-2, arr);
  return arr[n];
}