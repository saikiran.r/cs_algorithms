/* 
1664. Ways to Make a Fair Array
https://leetcode.com/problems/ways-to-make-a-fair-array/

*/

// solution 3: Best way o(n) time complexity and o(1) space.

var waysToMakeFair = function(nums) {
    
    let odd = 0;
    let even = 0;
    for(let i=0; i<nums.length; i++){
        if(i % 2 == 0){
            even += nums[i];
        } else {
            odd += nums[i];
        }
    };
    let oddLeft = 0;
    let evenLeft = 0;
    let count = 0;
    for(let i=0; i<nums.length; i++){
        let tempEven = 0,  tempOdd = 0;
        if(i % 2 == 0){
            
            tempOdd = even - (nums[i] + evenLeft);
            tempEven = odd - oddLeft;
            
            if (evenLeft + tempEven === oddLeft + tempOdd ){
            count++;
            }
            evenLeft += nums[i];
            
        } else {
            
            tempOdd = even - evenLeft;
            tempEven = odd - (nums[i] + oddLeft);
            
            if (evenLeft + tempEven === oddLeft + tempOdd ){
            count++;
            }
            
            oddLeft += nums[i];
        }
    }
    return count;
};





// solution 2: Optimal way o(n)
/**
 * @param {number[]} nums
 * @return {number}
 */
/*
Psudo code:

while removing element - before, element(a[i]), after
(i  % 2 == 0){
  after.odd = even - (a[i] + before.even)
  after.even = odd - before.odd
} else {
(i  % 2 == 0){
  after.odd = even -  before.even
  after.even = odd - (a[i] + before.odd)
}
if (before.even + after.even === before.odd + after.odd ){
  count++;
}

*/
var waysToMakeFair = function(nums) {
    
    let odd = 0;
    let even = 0;
    let oddEvenPairUntilIndex = [];
    for(let i=0; i<nums.length; i++){
        if(i % 2 == 0){
            even += nums[i];
        } else {
            odd += nums[i];
        }
        oddEvenPairUntilIndex.push({odd, even});
    };
    
    let count = 0;
    for(let i=0; i<nums.length; i++){
        let tempEven,  tempOdd;
        tempOdd = even - oddEvenPairUntilIndex[i].even;
        tempEven = odd - oddEvenPairUntilIndex[i].odd;
        if(i == 0){
            if(tempOdd === tempEven){
                count++;
            }
        } else {
            if (oddEvenPairUntilIndex[i-1].even + tempEven === oddEvenPairUntilIndex[i-1].odd + tempOdd ){
                count++;
            }
        }
    }
    return count;
};





// Solution 1: Brute force method
/**
 * @param {number[]} nums
 * @return {number}
 */
var waysToMakeFair = function(nums) {
    let count = 0
    for(let i=0; i<nums.length; i++){
        let obj1 = elements(nums, 0, i);
        let obj2 = elements(nums, i+1, nums.length);
        if(obj1.odd + obj2.odd == obj1.even + obj2.even){
            count++;
        }
    };
    return count;
};

function elements(arr, l, r){
    let odd = 0;
    let even = 0;
    if(l === 0){
        for(let i=0; i< r; i++){
            if(i % 2 === 0){
                even += arr[i]; //2
            } else{
                odd += arr[i];
            }
        }
    } else {
        for(let i=l; i< r; i++){
            if(i % 2 === 0){
                odd += arr[i]; //6
            } else{
                even += arr[i]; //4
            }
        }
    }
    return {
        odd, even
    }
}


// old is old and even is even
//after removing
// old is even and even is odd

