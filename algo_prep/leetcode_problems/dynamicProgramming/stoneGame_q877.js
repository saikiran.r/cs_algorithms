/*
877. Stone Game
https://leetcode.com/problems/stone-game/submissions/
*/

/**
 * @param {number[]} piles
 * @return {boolean}
 */
var stoneGame = function(piles) {
    let dp = [new Array(piles.length)];
    let n = piles.length;
    return go(0, n-1, piles, dp) > 0 ? 1: 0;
};

function go(low, high, arr, dp){
    
    if(low+1 == high)
        return Math.abs(arr[low] - arr[high]);
    if(!dp[low]){
        dp[low] = new Array(arr.length);
    }
    if(!dp[low][high]){
        let currentWin = 0;
        currentWin = arr[low] + go(low+1, high, arr, dp);
        currentWin = Math.max(currentWin, arr[high] + go(low, high-1, arr, dp));
        dp[low][high] = currentWin;
    }
    return dp[low][high];
}