/*
647. Palindromic Substrings
https://leetcode.com/problems/palindromic-substrings/
*/

//solution 3 - fastest from others

/**
 * @param {string} s
 * @return {number}
 */
var countSubstrings = function(s) {
  const l = s.length;
  if (l < 2) return l
  let c = 0;
  
  const ext = (a, b) => {
    while (a >= 0 && b < s.length && s[a] === s[b]) {
      c++;
      a--;
      b++;
    }
  };
  
  for (let i = 0; i < l; i++) {
    ext(i, i);
    ext(i, i + 1);
  }
  return c;
};


//solution 2 - using recursive and dp

/**
 * @param {string} s
 * @return {number}
 */
var countSubstrings = function(s) {
    
    let dp = [];
    let count = 0;
    for(let i = 0; i< s.length; i++) {
        for(j = i; j < s.length; j++) {
            count += go(i, j, s, dp);
        }
    }
    return count;
};

function go(i, j, s, dp){
    if(i > j) return 1;
    if(i == j) return 1;
    if(!dp[i]){
        dp[i] = new Array(s.length);
    }
    if(dp[i][j] == undefined){
        if(s[i] != s[j]) {
            dp[i][j] = 0;
        } else {
            dp[i][j] = go(i+1, j-1, s, dp);
        }
    }
    return dp[i][j];
}





//solution 1
/**
 * @param {string} s
 * @return {number}
 */
var countSubstrings = function(s) {
    if(s.length == 0){
        return 0;
    }
    let count = 0;
    for(let i=0; i< s.length; i++){ //N
        let j = 2;
        let str = s.slice(i, i+j)
        while(i+j <= s.length){//n
            if(isPalindrome(str) !== -1){
                count++;
            }
            j++;
            str = s.slice(i, i+j);
        }
    }
    return count + s.length;
};

function isPalindrome(str){
    if(str.length == 0){
        return -1;
    }
    let i = 0, j= str.length-1;
    while(i<=j){
        if(str[i] !== str[j]){
            return -1;
        }
        i++;
        j--;
    }
    return 1;
}