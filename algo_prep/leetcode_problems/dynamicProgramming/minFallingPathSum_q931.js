/*
931. Minimum Falling Path Sum
https://leetcode.com/problems/minimum-falling-path-sum/submissions/
*/
/**
 * @param {number[][]} A
 * @return {number}
 */



 /**
  * @param {number[][]} A
  * @return {number}
  */

// solution 1: without DP
 var minFallingPathSum = function(A) {
     let col = A[0].length;
     let ans = Number.MAX_SAFE_INTEGER;
     for(let j=0; j<col; j++){
         ans = Math.min(ans, go(0,j, A));
     }
     return ans;
 };

 //This function gets the min path from x,y to last element i.e. row.length-1,col.length-1
 function go(x,y, arr){
     let panalty = Number.MAX_SAFE_INTEGER / arr.length;
     if(x === arr.length){
         return 0;
     }
     if(y >= arr[0].length || y < 0){
         return panalty;
     }
     let minValue = Number.MAX_SAFE_INTEGER;
     
     minValue = arr[x][y] + Math.min(minValue, go(x+1, y, arr), go(x+1, y-1, arr), go(x+1, y+1, arr));
     
     return minValue;
     
 }


 /* Solution 2: Now using DP below */

 var minFallingPathSum = function(A) {
    let row = A.length;
    let col = A[0].length;
    
    var dp = [];
    
    let ans = Number.MAX_SAFE_INTEGER;
    for(let j=0; j<col; j++){
        ans = Math.min(ans, go(0,j, A, dp));
    }
    return ans;
};

//This function gets the min path from x,y to last element i.e. row.length-1,col.length-1
function go(x,y, arr, dp){
    debugger;
    let panalty = Number.MAX_SAFE_INTEGER / arr.length;
    if(x === arr.length){
        return 0;
    }
    if(y >= arr[0].length || y < 0){
        return panalty;
    }
    let minValue = Number.MAX_SAFE_INTEGER;
    if(!dp[x]){
        dp[x] = new Array(arr[0].length);
    }
    if(!dp[x][y]){
        dp[x][y] = minValue;
        dp[x][y] = arr[x][y] + Math.min(dp[x][y], go(x+1, y, arr, dp), go(x+1, y-1, arr, dp), go(x+1, y+1, arr, dp));
    }
    
    return dp[x][y];
    
}



// Solution 3: By mutating the input Array
var minFallingPathSum = function(A) {
    let row = A.length;
    let col = A[0].length;
    
    let ans = Number.MAX_SAFE_INTEGER;
    for(let x=row - 2; x >= 0; x--){ 
        for(let y=0; y<col; y++){
            let best = A[x+1][y];
            if(y > 0) best = Math.min(best, A[x+1][y-1]);
            if(y+1 < col) best = Math.min(best, A[x+1][y+1]);
            A[x][y] +=best;
        }
        
    }
    
    ans = Math.min(ans, ...A[0]); // min from a[0] array elements
    
    return ans;
};