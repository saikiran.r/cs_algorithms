/*
https://leetcode.com/problems/counting-bits/
*?
// My solution
// O(n * i)
/**
 * @param {number} num
 * @return {number[]}
 */
var countBits = function(num) {
    dp = [0];
    for(let i= num; i>=0; i--){ //N
        countOnes(i, dp);
    }
    return dp;
};


function countOnes(n, dp){
    if(n == 0){
        return dp[n];
    }
    if(!dp[n]){
        dp[n] = n%2 + countOnes(Math.floor(n/2), dp); //0+
    }
    return dp[n];
}

//another way to ccounting ones - Bit magic
function countOnes(x, dp){
    let ans = 0;
    if(!dp[x]){
        while(x){
            ans++,
            x -= x & -x;
        }
        dp[x] = ans;
    }
    return dp[x];
}



//Optimal sol
/**
 * @param {number} num
 * @return {number[]}
 */
var countBits = function(num) {
   
    if(num==0)return [0]
    let res = [0,1]
    let m = 2
    let j=0
    for(let i=2;i<=num;i++){
        if(j==m){
            j=0
            m=i
        }
        res[i]=1+res[j]
        j++
    }
    // console.log(res)
    return res
};