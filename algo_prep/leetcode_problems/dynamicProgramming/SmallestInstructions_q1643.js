/* 

1643. Kth Smallest Instructions
https://leetcode.com/problems/kth-smallest-instructions/

*/

/**
 * @param {number[]} destination
 * @param {number} k
 * @return {string}
 */
var kthSmallestPath = function(destination, kk) {
    let noOfH = BigInt(destination[1]);
    let noOfV = BigInt(destination[0]);
    let k = BigInt(kk);
    //factorial(noOfH + noOfV) / (factorial(noOfH) * factorial(noOfV);
    
    var str = '';
    while(noOfH > 0 && noOfV > 0){
        //place H in the first place. so, the resultant combinations are n+r-1Cr
        let pre = comb(noOfH + noOfV - BigInt(1), noOfV); 
        if(k <= pre){
            str += 'H';
            noOfH -= BigInt(1);
        } else {    
            str += 'V';
            noOfV -= BigInt(1);
            k -= pre;
        }
    }
    if(noOfH == 0){
        str += printLetter(noOfV, 'V');
    }
    if(noOfV == 0){
        str += printLetter(noOfH, 'H');
    }
    return str;
};

function comb(n, r){
    return factorial(n) / (factorial(r) * factorial(n-r));
}

function printLetter(n, letter){
    let str = new Array(n);
    for(i = 0; i<n; i++){
        str[i] = letter;
    }
    return str.join('');
}

function fact(){
    let cache = {0: BigInt(1), 1: BigInt(1)};
    return function(n){
        if(!cache[n]){
            cache[n] = n * factorial(n - BigInt(1));
        }
        return cache[n];
    }
}

const factorial = fact();