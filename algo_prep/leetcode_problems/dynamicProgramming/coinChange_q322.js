/*
https://leetcode.com/problems/coin-change/submissions/
322. Coin Change
*/
//best sol in leetcode
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
const coinChange = (coins, amount) => {
  coins.sort((a, b) => b - a);

  let res = Infinity;

  const find = (k, amount, count) => {
    const coin = coins[k];

    // last smallest coin
    if (k === coins.length - 1) {
      if (amount % coin === 0) {
        res = Math.min(res, count + ~~(amount / coin));
      }
    } else {
      for (let i = ~~(amount / coin); i >= 0 && count + i < res; i--) { // count + i < res is for pruning, avoid unnecessary calculation
        find(k + 1, amount - coin * i, count + i);
      }
    }
  };

  find(0, amount, 0);
  return res === Infinity ? -1 : res;
};




//solution 2: Iterative way

/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function(coins, amount) {
    let dp = [];
    let n = coins.length;
    // dp[i][j] is min coins needed to make sum "i" when first "j" coins are available
    for(let i=0 ; i<=amount; i++){
        dp[i] = [];
        for(let j=0; j<=n; j++){
            dp[i][j] =  Infinity;
        }
    }
    debugger;//[0, 0]
    dp[0][0] = 0; // base case when no. of ccoins is 0 and amount is 0;
    for(let i=0 ; i<=amount; i++){
        for(let j=1; j<=n; j++){
            let currentCoin = coins[j-1];//0-based indexing

            //# Case A: I don't take jth coin
            dp[i][j] = dp[i][j-1];

            //# Case B: I take jth coin
            if(currentCoin <= i){
                dp[i][j] = Math.min(dp[i][j], 1 + dp[i-currentCoin][j]); //0, 0
            }
            
        }
    }
    
    return dp[amount][n] === Infinity ? -1 : dp[amount][n];
};



//Solution 1: space complexity 0(amount * coins.length) i.e no of states in dp and the depth if the recursive, 


/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function(coins, amount) {
    let dp = [];
    let result = go(0, coins, amount, dp);
    if(result >= Number.MAX_SAFE_INTEGER){
        return -1;
    } else {
        return result;
    }
};

function go(i, coins, amount, dp){
    let max = Number.MAX_SAFE_INTEGER;
    
    if(amount === 0){
        return 0;
    }
    if(i >= coins.length){ // 1
        return max;
    }
    if(amount < 0){
        return max;
    }
    if(!dp[i]){
        dp[i] = [];
    }
    if(!dp[i][amount]){
        dp[i][amount] = go(i+1, coins, amount, dp);
    
        dp[i][amount] = Math.min(dp[i][amount],  1 + go(i, coins, amount - coins[i], dp)); //0, 0
        
    }
    return dp[i][amount];
    
}