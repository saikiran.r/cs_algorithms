/*
53. Maximum Subarray
https://leetcode.com/problems/maximum-subarray/

*/
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
    debugger;
    if(nums.length === 1){
        return nums[0];
    }
    let currentMax = -Number.MAX_VALUE;
    let prevMax = -Number.MAX_VALUE, ans = -Number.MAX_VALUE;
    for(i = 0; i<nums.length;  i++){
        currentMax = Math.max(prevMax+nums[i], nums[i]);
        ans = Math.max(ans, currentMax); 
        prevMax = currentMax; 
    }
    return ans;
};

// Calculate the currentMax from max of prevMax+num[i] and num[i]
// overallMax is calculated in sum object.