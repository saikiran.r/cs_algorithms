Basics of dynamic programming

Step 1: see a pattern which is recursive 
Step 2: write a recursive function with a base function in it. (go function)
Step 3: Add a dp variable (mostly object/array) to store the results and pick them as we need them. The order of insert and get should be O(1).


List of other algorithms while using dp
1. recursive
2. matrix exponentional  - http://codeforces.com/contest/166/submission/1390234
3. Fenwick Trees