//Even Fibonacci numbers

function primeNumbers(n){
  let primeNumbers = [2]
  let i = 3;
  let result = 0;
  let sum = 0;
  while(sum < n){
    if(isPrime(i)){
    debugger;
      primeNumbers.push(i);
      sum = sumOfArray(primeNumbers)
      if(isPrime(sum) && sum < n){
        result = sum;
      }
    }
    i++;
  }
  return result;
}

function isPrime(n) {
  let prime = true;
  for(let i = 2; i<n; i++){
    if(n % i == 0){
      prime = false;
      break;
    }
  }
  return prime;
}

function sumOfArray(arr){
  let sum = 0;
  arr.forEach(i => {sum += i});
  return sum;
}